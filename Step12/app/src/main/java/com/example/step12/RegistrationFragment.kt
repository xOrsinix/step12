package com.example.step12

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.navigation.Navigation
import com.google.android.material.bottomnavigation.BottomNavigationView


class RegistrationFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_registration, container, false)
        view.findViewById<BottomNavigationView>(R.id.bNavReg).isVisible=false
        view.findViewById<Button>(R.id.btReg).setOnClickListener{
            if (view.findViewById<EditText>(R.id.etPasswordReg).text.toString()==view.findViewById<EditText>(R.id.etPasswordReg2).text.toString())
                Navigation.findNavController(view).navigate(R.id.toLoginFragment)
            else
                Toast.makeText(view.context,"Incorrect passwords",Toast.LENGTH_SHORT).show()
        }

        return view
    }

    companion object {
        fun newInstance() =
            RegistrationFragment()
    }
}