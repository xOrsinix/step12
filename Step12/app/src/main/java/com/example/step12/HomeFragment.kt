package com.example.step12

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import com.google.android.material.bottomnavigation.BottomNavigationView


class HomeFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_home, container, false)
        view.findViewById<BottomNavigationView>(R.id.bNavHome).selectedItemId = R.id.homeLabel
        view.findViewById<BottomNavigationView>(R.id.bNavHome).setOnNavigationItemSelectedListener {
            when (it.itemId){
                R.id.homeLabel -> {}

                R.id.logLabel -> {
                    Navigation.findNavController(view).navigate(R.id.fromHomeToLogin)
                }

                R.id.webLabel->{
                    Navigation.findNavController(view).navigate(R.id.toWebFragment)
                }
            }
            true
        }
        return view
    }

    companion object {

        fun newInstance() = HomeFragment()
    }
}