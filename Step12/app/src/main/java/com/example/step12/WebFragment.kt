package com.example.step12

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.Navigation
import com.google.android.material.bottomnavigation.BottomNavigationView


class WebFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_web, container, false)
        view.findViewById<BottomNavigationView>(R.id.bNavWeb).selectedItemId = R.id.webLabel
        view.findViewById<BottomNavigationView>(R.id.bNavWeb).setOnNavigationItemSelectedListener {
            when (it.itemId){
                R.id.webLabel -> { }

                R.id.logLabel -> {
                    Navigation.findNavController(view).navigate(R.id.fromWebToLogin)
                }

                R.id.homeLabel->{
                    Navigation.findNavController(view).navigate(R.id.fromWebToHome)
                }
            }
            true
        }
        return view
    }

    companion object {
        fun newInstance() =
            WebFragment()
            }
}